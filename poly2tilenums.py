#!/usr/bin/python3

import math
import sys

from enum import Enum
from osgeo import ogr

# Can be used to check that the tiles cover the region
# Note, graph window must be closed before program completes
SHOW_PLOT = False

BBOX_BUFFER = 0.1

class PolyFile:
    ZOOM=8
    NUMTILES=256
    END = "END"

    class Mode(Enum):
        OUTER = 0
        REGION = 1
        POLY = 2
        POINTS = 3

    def __init__(self, filename : str):
        self.polygons = []
        # only used for the slightly hacky plot tiles feature
        self.poly_array = [[]]
        self.min_x = None
        self.min_y = None
        self.max_x = None
        self.max_y = None
        self.min_lon = None
        self.min_lat = None
        self.max_lon = None
        self.max_lat = None
        self.parse_file(filename)

    # https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#Derivation_of_tile_names
    def lon_to_x(self, lon : float) -> float:
        return self.NUMTILES * (lon + 180) / 360

    # https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#Derivation_of_tile_names
    def lat_to_y(self, lat : float) -> float:
        return (self.NUMTILES * (1
                                 - (math.log(math.tan(math.radians(lat))
                                             + 1 / math.cos(math.radians(lat)))
                                    / math.pi))
                              / 2)

    def update_minmax_lon(self, lon : float):
        self.min_lon = (lon if self.min_lon is None
                        else min(lon, self.min_lon))
        self.max_lon = (lon if self.max_lon is None
                        else max(lon, self.max_lon))

        x = self.lon_to_x(lon)
        self.min_x = (x if self.min_x is None
                        else min(x, self.min_x))
        self.max_x = (x if self.max_x is None
                        else max(x, self.max_x))

    def update_minmax_lat(self, lat : float):
        self.min_lat = (lat if self.min_lat is None
                        else min(lat, self.min_lat))
        self.max_lat = (lat if self.max_lat is None
                        else max(lat, self.max_lat))

        y = self.lat_to_y(lat)
        self.min_y = (y if self.min_y is None
                        else min(y, self.min_y))
        self.max_y = (y if self.max_y is None
                        else max(y, self.max_y))

    def parse_file(self, filename : str):
        mode = self.Mode.OUTER
        current_line = None
        x = None
        y = None

        with open(filename) as f:
            for line in f:
                for word in line.split():
                    if mode == self.Mode.OUTER:
                        # move straight into region
                        mode = self.Mode.REGION
                    if mode == self.Mode.REGION:
                        if word.lower() == self.END.lower:
                            mode = self.Mode.OUTER
                        else:
                            # ignore name, move into poly
                            mode = self.Mode.POLY
                    elif mode == self.Mode.POLY:
                        # ignore name, start reading points
                        current_line = ogr.Geometry(ogr.wkbLinearRing)
                        mode = self.Mode.POINTS
                    elif mode == self.Mode.POINTS:
                        if word.lower() == self.END.lower():
                            polygon = ogr.Geometry(ogr.wkbPolygon)
                            polygon.AddGeometry(current_line)
                            polygon.CloseRings()
                            self.polygons.append(polygon)
                            self.poly_array.append([])
                            current_line = ogr.Geometry(ogr.wkbLinearRing)
                            mode = self.Mode.REGION
                        elif x is None:
                            lon = float(word)
                            self.update_minmax_lon(lon)
                            x = self.lon_to_x(lon)
                        else:
                            lat = float(word)
                            self.update_minmax_lat(lat)
                            y = self.lat_to_y(lat)

                            current_line.AddPoint(x, y)
                            self.poly_array[-1].append((x, y))
                            x = None
                            y = None

    def intersects(self, poly : ogr.Geometry) -> bool:
        """True if poly intersects with polys in the file
        :param poly: an ogr.Geometry of type ogr.wkbPolygon
        :return: true if poly intersects with some poly in the file"""
        for fpoly in self.polygons:
            if not fpoly.Intersection(poly).IsEmpty():
                return True
        return False

    def get_left(self) -> float:
        """Leftmost x value in file polygons"""
        return self.min_x

    def get_right(self) -> float:
        """Rightmost x value in file polygons"""
        return self.max_x

    def get_top(self) -> float:
        """Topmost x value in file polygons"""
        return self.min_y

    def get_bottom(self) -> float:
        """Bottommost y value in file polygons"""
        return self.max_y

    def get_west_lon(self) -> float:
        """Westmost lon value in file polygons"""
        return self.min_lon

    def get_east_lon(self) -> float:
        """Eastmost lon value in file polygons"""
        return self.max_lon

    def get_north_lat(self) -> float:
        """Northmost lat value in file polygons"""
        return self.max_lat

    def get_south_lat(self) -> float:
        """Southmost lat value in file polygons"""
        return self.min_lat


    def __str__(self) -> str:
        return "\n".join(str(p) for p in self.polygons)


def make_square(top : float, bottom : float,
                left : float, right : float) -> ogr.Geometry:
    """Return polygon representing a square
    :param top, bottom, left, right: sides of square
    :return: a wkbPolygon geometry"""
    line = ogr.Geometry(ogr.wkbLinearRing)
    line.AddPoint(left, top)
    line.AddPoint(right, top)
    line.AddPoint(right, bottom)
    line.AddPoint(left, bottom)
    poly = ogr.Geometry(ogr.wkbPolygon)
    poly.AddGeometry(line)
    poly.CloseRings()
    return poly

def process_file(filename : str):
    pf = PolyFile(filename)

    top = math.floor(pf.get_top())
    bottom = math.ceil(pf.get_bottom())
    left = math.floor(pf.get_left())
    right = math.ceil(pf.get_right())

    pts = []

    # output bbox
    print(pf.get_south_lat() - BBOX_BUFFER,
          pf.get_west_lon() - BBOX_BUFFER,
          pf.get_north_lat() + BBOX_BUFFER,
          pf.get_east_lon() + BBOX_BUFFER)

    # output tiles
    for x in range(left, right + 1):
        for y in range(top, bottom + 1):
            tile = make_square(y, y + 1, x, x + 1)
            if pf.intersects(tile):
                print("{}:{}".format(x, y))
                pts.append((x, y))

    if SHOW_PLOT:
        import numpy as np
        import matplotlib.pyplot as plt

        for p in pf.poly_array:
            if len(p) > 0:
                poly = plt.Polygon(p,closed=True,edgecolor='blue',alpha=0.4)
                plt.gca().add_patch(poly)

        for (x, y) in pts:
            poly = plt.Polygon([(x,y),(x,y+1),(x+1,y+1),(x+1,y)],
                                closed=True,edgecolor='green',alpha=0.4)
            plt.gca().add_patch(poly)

        plt.gca().invert_yaxis()
        plt.axis('scaled')
        plt.show()

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: {} poly_file".format(sys.argv[0]))
        print()
        print("Outputs:")
        print("  First line: 'bottom left top right' bbox lat/lon with {.2} buffer".format(BBOX_BUFFER))
        print("  Each other line: 'x:y' tile numbers at zoom level 8 needed to cover poly")
        exit(-1)

    filename = sys.argv[1]
    process_file(filename)

