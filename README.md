# Wahoo ELEMNT Bolt Maps Creator

This is a proof-of-concept adaptation of
[mapsforge-creator](https://github.com/mapsforge/mapsforge-creator) to
produce map files for the Wahoo ELEMNT Bolt.

The script downloads OpenStreetMap data from [Geofabrik](https://download.geofabrik.de/) and land polygons from [OpenStreetMap Data](https://osmdata.openstreetmap.de/) and produces tiled .map.lzma files compatible with the Bolt.

## Requirements

* [Osmosis](https://wiki.openstreetmap.org/wiki/Osmosis).
    * Download the **map-writer** plugins (**jar-with-dependencies**),
      either their release version from [Maven
      Central](https://search.maven.org/search?q=g:org.mapsforge) or
      their snapshot version from [Sonatype OSS Repository
      Hosting](https://oss.sonatype.org/content/repositories/snapshots/org/mapsforge/).
      See the Osmosis
      [documentation](https://wiki.openstreetmap.org/wiki/Osmosis/Detailed_Usage#Plugin_Tasks)
      for how to properly install them.
* [GDAL](https://gdal.org/)
* [Java](https://www.java.com/)
* [Python 3.x](https://www.python.org/)
* Python GDAL bindings (`python -m pip install GDAL`)
* [lzma](https://linux.die.net/man/1/lzma) (or gzip)
* [adb](https://developer.android.com/studio/command-line/adb) to clear
  maps cache (or else use the Wahoo Android app).

## Configure

The script has some config specifications in `Configuration` section at
the top. Adjust them to your environment.

You could increase the Java heap space that may be allocated for
Osmosis. You can do so by setting the global variable
`JAVACMD_OPTIONS=-Xmx1024M`. This sets the maximum available Java heap
space to 1024MB. Of course you can set this parameter to a value which
fits best for your purpose.

## Running

Run `bolt-maps-creator.sh` script without arguments to see its usage.

For example

    $ bolt-maps-creator.sh europe/great-britain/england hd en

to create tiles covering England, using hard-disk instead of RAM, and
English language.

You can see available regions on the [Geofabrik download
page](http://download.geofabrik.de/).

It takes some time to run.

## Results

The map tiles are written to

    $MAPS_PATH/<version>/<region>

and stored in the struture `8/x/y.map.lzma`.

## Installation on Wahoo

There are two steps

1. Copy files to device.
2. Clear device map cache.

To copy files to the device (all below at your own risk, though you can
always reinstall old maps with the Android app):

1. Mount the device via MTP. I use [jmtpfs](https://github.com/JasonFerrara/jmtpfs).
2. Open the `USB storage/maps` directory.
3. Clear the `USB storage/maps/temp` directory (or the whole maps
   directory).
4. Copy the contents of `$MAPS_PATH/<version>/<region>` to the `USB
   storage/maps/tiles` directory.
    * I.e. end up with `USB storage/maps/tiles/8/x/y.map.lzma`
      for all `x` and `y` in the region you downloaded
5. Reboot the device.

To clear the maps cache via ADB:

1. Probably you shouldn't be viewing the map page when you do this.
2. Unlock ADB on the device by pressing "power" and "up" and the same
   time.
    * See [here](https://joshua0.dreamwidth.org/66991.html)
3. Run `adb shell` to get shell access.
4. At this point you're really operating at your own risk.
5. Delete the directory
   `/data/user/0/com.wahoofitness.bolt/files/maps-cache`.
6. View your new map

To clear the maps cache via the Bolt app you can (apparently) start
downloading any new region (e.g. Antarctica) and cancel after 5-10s
(thanks to [Wandrer.earth](https://wandrer.earth/) for the tip).

As a bonus, the maps-cache contains .tile files which are actually PNGs
-- you can edit these if you want to draw smiley faces on your maps.

## Limitations

### Maps size

The current set up of the script creates maps that are more
detailed than the standard Wahoo maps.

* More POIs (not shown)
* Line simplification applied but doesn't quite match Wahoo's, which
  might be custom.

For comparison, England is 81mb officially, but 107mb from this script.
The `NODE_TAGS`, `WAY_TAGS`, and `REGION_TAGS` can be tweaked.

### Maps Borders

Wahoo maps divide the world into 256x256 tiles. Any region you download
will not fill these tiles perfectly. Hence, the edge tiles will only be
partially filled. If you copy these tiles over existing tiles, you will 
lose data from a different region that occupies the same tile.

To see what tiles are being generated, set SHOW_PLOT to True in
poly2tilenums.py.

## Why do this?

To see if you can!

Also, to update maps without installing the Wahoo app.
