#!/bin/bash

# Automatic generation for Mapsforge maps in tiles for the Wahoo ELEMNT
# Bolt with OpenStreetMap data from Geofabrik
#
# adapted from map-creator.sh by devemux86

# Configuration

PYTHON3="/usr/bin/python3"

[ $OSMOSIS_HOME ] || OSMOSIS_HOME="/usr"
[ $DATA_PATH ] || DATA_PATH="$HOME/mapsforge/data"
[ $MAPS_PATH ] || MAPS_PATH="$HOME/mapsforge/maps"

# Can also be gzip
[ $COMPRESS_CMD ] || COMPRESS_CMD="/usr/bin/lzma"
[ $COMPRESS_FILE_EXT ] || COMPRESS_FILE_EXT="lzma"

[ $DAYS ] || DAYS="30"
[ $LAND_DAYS ] || LAND_DAYS="365"

[ $TAG_VALUES ] || TAG_VALUES="false"
[ $COMMENT ] || COMMENT="Map data (c) OpenStreetMap contributors"

[ $NODE_TAGS ] || NODE_TAGS="highway=* place=*"
[ $REJECT_NODE_TAGS ] || REJECT_NODE_TAGS="highway=bus_stop"
[ $WAY_TAGS ] || WAY_TAGS="highway=* railway=* natural=water waterway=* boundary=administrative natural=nosea aeroway=* natural=sea aeroway=taxiway barrier=* water=*"
[ $RELATION_TAGS ] || RELATION_TAGS=$WAY_TAGS

SIMPLIFICATION_FACTOR=130
SIMPLIFICATION_MAX_ZOOM=20

# =========== DO NOT CHANGE AFTER THIS LINE. ===========================
# Below here is regular code, part of the file. This is not designed to
# be modified by users.
# ======================================================================

if [ $# -lt 1 ]; then
    echo "Usage: $0 continent/country[/region] [ram|hd] [lang1,...,langN] [1...N]"
    echo "Example: $0 europe/germany/berlin ram en,de,fr,es 1"
    exit
fi

cd "$(dirname "$0")"

NAME="$(basename "$1")"

WORK_PATH="$DATA_PATH/$1"

if [ "$TAG_VALUES" = "true" ]; then
    MAPS_PATH="$MAPS_PATH/v5"
else
    [ $3 ] && MAPS_PATH="$MAPS_PATH/v4" || MAPS_PATH="$MAPS_PATH/v3"
fi

# Pre-process

mkdir -p "$WORK_PATH"
mkdir -p "$MAPS_PATH"

# Download data

REGION_FILE="$WORK_PATH/$NAME-latest.osm.pbf"

if [ -f "$REGION_FILE" ] && [ $(find "$REGION_FILE" -mtime -$DAYS) ]; then
    echo "$REGION_FILE exists and is newer than $DAYS days."
else
    echo "Downloading $1..."
    wget -nv -N -P "$WORK_PATH" https://download.geofabrik.de/$1-latest.osm.pbf || exit 1
    wget -nv -N -P "$WORK_PATH" https://download.geofabrik.de/$1-latest.osm.pbf.md5 || exit 1
    (cd "$WORK_PATH" && exec md5sum -c "$NAME-latest.osm.pbf.md5") || exit 1
    wget -nv -N -P "$WORK_PATH" https://download.geofabrik.de/$1.poly || exit 1
fi

# Download land

if [ -f "$DATA_PATH/land-polygons-split-4326/land_polygons.shp" ] && [ $(find "$DATA_PATH/land-polygons-split-4326/land_polygons.shp" -mtime -$LAND_DAYS) ]; then
    echo "Land polygons exist and are newer than $DAYS days."
else
    echo "Downloading land polygons..."
    echo $DATA_PATH/land-polygons-split-4326/land_polygons.shp
    rm -rf "$DATA_PATH/land-polygons-split-4326"
    rm -f "$DATA_PATH/land-polygons-split-4326.zip"
    wget -nv -N -P "$DATA_PATH" https://osmdata.openstreetmap.de/download/land-polygons-split-4326.zip || exit 1
    unzip -oq "$DATA_PATH/land-polygons-split-4326.zip" -d "$DATA_PATH"
fi

# Bounds

POLY_FILE="$WORK_PATH/$NAME.poly"

BOUNDS=($($PYTHON3 poly2tilenums.py "$POLY_FILE"))

BOTTOM=${BOUNDS[0]}
LEFT=${BOUNDS[1]}
TOP=${BOUNDS[2]}
RIGHT=${BOUNDS[3]}

TILES=${BOUNDS[@]:4}

# Land

ogr2ogr -overwrite -progress -skipfailures -clipsrc $LEFT $BOTTOM $RIGHT $TOP "$WORK_PATH/land.shp" "$DATA_PATH/land-polygons-split-4326/land_polygons.shp"
$PYTHON3 shape2osm.py -l "$WORK_PATH/land" "$WORK_PATH/land.shp"

# Sea

cp sea.osm "$WORK_PATH"
sed -i "s/\$BOTTOM/$BOTTOM/g" "$WORK_PATH/sea.osm"
sed -i "s/\$LEFT/$LEFT/g" "$WORK_PATH/sea.osm"
sed -i "s/\$TOP/$TOP/g" "$WORK_PATH/sea.osm"
sed -i "s/\$RIGHT/$RIGHT/g" "$WORK_PATH/sea.osm"

# Merge

CMD="$OSMOSIS_HOME/bin/osmosis --rb file=$WORK_PATH/$NAME-latest.osm.pbf \
                             --rx file=$WORK_PATH/sea.osm --s --m"
for f in $WORK_PATH/land*.osm; do
    CMD="$CMD --rx file=$f --s --m"
done
CMD="$CMD --wb file=$WORK_PATH/merge.pbf omitmetadata=true"
echo $CMD
eval "$CMD" || exit 1


# Zoom level 8
ZOOM=8
NUMTILES=256

# Write tiles
for tile in $TILES
do
    [[ ! $tile =~ ([0-9]*):([0-9]*) ]] && continue

    x=${BASH_REMATCH[1]}
    y=${BASH_REMATCH[2]}

    TILE_DIR=$ZOOM/$x
    TILE_NAME="$TILE_DIR/$y"

    mkdir -p "$WORK_PATH/$TILE_DIR"

    # calc coords (https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#Derivation_of_tile_names)
    L=$($PYTHON3 -c "print($x / $NUMTILES * 360 - 180)")
    R=$($PYTHON3 -c "print($((x+1)) / $NUMTILES * 360 -180)")
    T=$($PYTHON3 -c "import math; print(math.degrees(math.atan(math.sinh(math.pi * (1 - 2 * $y / $NUMTILES)))))")
    B=$($PYTHON3 -c "import math; print(math.degrees(math.atan(math.sinh(math.pi * (1 - 2 * $((y+1)) / $NUMTILES)))))")

    CENTER_LON=$($PYTHON3 -c "print(($L + $R) / 2.0)")
    CENTER_LAT=$($PYTHON3 -c "print(($T + $B) / 2.0)")

    BOUNDING_BOX="--bounding-box x1=$x x2=$x y1=$y y2=$y zoom=$ZOOM"

    READ_FILE="--read-pbf file=$WORK_PATH/merge.pbf"
    TRANSFORM_ARGS=""
    [ $MAP_TRANSFORM_FILE ] && TRANSFORM_ARGS="--tt file=$MAP_TRANSFORM_FILE"
    CMD="$OSMOSIC_HOME/bin/osmosis \
            $READ_FILE \
                $BOUNDING_BOX \
                --tag-filter reject-relations \
                --tag-filter reject-ways \
                --tag-filter accept-nodes $NODE_TAGS \
                --tag-filter reject-nodes $REJECT_NODE_TAGS \
            \
            $READ_FILE \
                $BOUNDING_BOX \
                --tag-filter reject-relations \
                --tag-filter accept-ways $WAY_TAGS \
                --used-node \
            \
            $READ_FILE \
                $BOUNDING_BOX \
                --tag-filter accept-relations $RELATION_TAGS \
                --used-way \
                --used-node \
            \
            --merge --merge \
            $TRANSFORM_ARGS \
            --mw file=$WORK_PATH/$TILE_NAME.map \
                 bbox=$B,$L,$T,$R \
                 map-start-position=$CENTER_LAT,$CENTER_LON \
                 map-start-zoom=8 \
                 simplification-factor=$SIMPLIFICATION_FACTOR \
                 simplification-max-zoom=$SIMPLIFICATION_MAX_ZOOM \
                 tag-values=$TAG_VALUES \
                 comment=\"$COMMENT\""
    [ $2 ] && CMD="$CMD type=$2"
    [ $3 ] && CMD="$CMD preferred-languages=$3"
    [ $4 ] && CMD="$CMD threads=$4"
    [ $MAP_TAG_CONF_FILE ] && CMD="$CMD tag-conf-file=$MAP_TAG_CONF_FILE"
    echo $CMD
    eval "$CMD" || exit 1

    # Compress and move

    $COMPRESS_CMD "$WORK_PATH/$TILE_NAME.map"
    mkdir -p "$MAPS_PATH/$TILE_DIR"
    mv "$WORK_PATH/$TILE_NAME.map.$COMPRESS_FILE_EXT" "$MAPS_PATH/$TILE_NAME.map.$COMPRESS_FILE_EXT"
done
